# Desafio FrontEnd - Player 2

Olá, obrigado por participar de nosso processo seletivo, sabemos que é bastante difícil encontrar tempo, principalmente quando se está trabalhando, por isso, nosso muito obrigado pelo seu tempo e dedicação.

O código produzido deve estar versionado em algum repositório público (Github, Bitbucket etc.).

Quando estiver tudo pronto, por favor, envie um e-mail para sejaum@player2.tech com o assunto "[Player 2] - Desafio FrontEnd - <%SEU_NOME%> " e o link para o seu repositório.

## Conhecimentos necessários para a execução do desafio

- [ ] ReactJS
- [ ] HTML
- [ ] CSS (SASS, Styled Components...)
- [ ] Gerenciamento de Estado (Redux, MobX...)
- [ ] Consumir APIs externas
- [ ] Responsividade
- [ ] Rotas
- [ ] Autenticação

Diferencial:
- [ ] Docker
- [ ] Web APIs
- [ ] NoSQL
- [ ] Typescript

## Por onde começar
Esse desafio tem como intuito que você mostre seus conhecimentos em criar uma interface front-end que tenha uma boa experiência de usuário e se integre com os serviços necessários.

## Requisitos
O designer projetou a UI e está no, você deve criar um projeto em ReactJS a partir desse layout.

- [ ] A interface deve ser responsiva.
- [ ] A listagem de bancos deve ser consumida da [Brasil API](https://brasilapi.com.br/docs#tag/BANKS)
- [ ] O Login pode ser simulado, não é necessário autenticar em uma API real, porém, você deve salvar um Token e salvar no Gerenciador de Estado(Redux) os dados do usuário. Ou seja, devem haver rotas públicas e privadas.

## Link do Design
[Link do Figma](https://www.figma.com/file/QXa5TNz53mCgHjcjx268E7/Untitled?node-id=0%3A1)
Faça login(ou crie) com sua conta do Figma que você poderá inspecionar os elementos.

Você não precisa subir o sistema online, porém subir em algum serviço em nuvem é interessante.

## Final
Quando estiver tudo pronto, por favor, envie um e-mail para sejaum@player2.tech com o assunto "[Player 2] - Desafio FrontEnd - <%SEU_NOME%> " e o link para o seu repositório e agendarmos uma call para você apresentar.

## Dúvidas
Mande um whatsapp para

(11) 9-3379-6168 - Fábio

(71) 9-9979-6332 - André Batista

(75) 9-9259-5185 - Joabe Fernandes
